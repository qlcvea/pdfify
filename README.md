# pdfify

A tool to bundle up files as attachments to a PDF document.

---

I made this tool because I need to send in varied files via a ticketing system
that only accepts a single .pdf or .doc (not-x) file as an attachment.

Currently deployed at <https://pdfify.nast.ro>.

## Build instructions

`node` and `npm` are required for building and development.

- Production build: `npm run build`
- Development server: `npm run dev`
