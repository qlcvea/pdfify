import licensesList from 'licenses-list-generator';
import fs from 'fs';

console.log('creating license bundle file');
let licenseFile = fs.openSync('dist/licenses.txt', 'w');
let isFirst = true;
let endsWithNewline = false;
for (let license of licensesList()) {
  if (isFirst) {
    isFirst = false;
  } else {
    if (!endsWithNewline) { fs.writeSync(licenseFile, '\n'); }
    fs.writeSync(licenseFile, '\n--------------------------------------------------------------------------------\n\n');
  }
  fs.writeSync(licenseFile, `${license.name} ${license.version}\nLicense: ${license.type}\n\n${license.text}`);
  endsWithNewline = license.text.endsWith('\n');
}
if (!endsWithNewline) { fs.writeSync(licenseFile, '\n'); }
fs.closeSync(licenseFile);
console.log('finished creating license bundle file');
