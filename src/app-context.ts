import { createContext } from "preact";
import Attachment from "./tool/attachment";
import DocumentInitializer from "./tool/document-initializer/document-initializer";
import WebDocumentInitializer from "./web-document-initializer";

type AppContext = {
  attachments: Attachment[];
  documentInitializer: DocumentInitializer
};

type AppContextApi = {
  setContext: (context: AppContext) => void;
}

type AppContextContent = AppContext & AppContextApi;

const AppContext = createContext<AppContextContent>({
  attachments: [],
  documentInitializer: new WebDocumentInitializer(),
  setContext: () => { throw new Error('Not implemented'); }
});

export default AppContext;

export type { AppContext, AppContextApi, AppContextContent };