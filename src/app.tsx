import { useRef, useState } from "preact/hooks"
import AppContext, { AppContextContent } from "./app-context";
import AttachmentsConfigurator from "./configurator/attachments-configurator";
import DocumentInitializerPicker from "./configurator/document-initializer/document-initializer-picker";
import FileDocumentInitializerConfigurator from "./configurator/document-initializer/file-document-initializer-configurator";
import WebDocumentInitializerConfigurator from "./configurator/document-initializer/web-document-initializer-configurator";
import { Header } from "./header"
import FileDocumentInitializer from "./tool/document-initializer/templated-document-initializer";
import pdfify from "./tool/pdfify";
import WebDocumentInitializer from "./web-document-initializer";

export function App() {
  const apiContextSetter = (context: AppContext) => { setContext({ ...context, setContext: apiContextSetter }); };
  const [context, setContext] = useState<AppContextContent>({
    attachments: [],
    documentInitializer: new WebDocumentInitializer(),
    setContext: apiContextSetter
  });

  return (
    <>
      <Header />
      <form onSubmit={async (e) => {
        e.preventDefault();

        const dialog = window.document.createElement('dialog');
        const text = window.document.createElement('p');
        text.innerText = 'Generating';
        dialog.appendChild(text);
        window.document.body.appendChild(dialog);
        dialog.showModal();

        const document = await pdfify(context.attachments, context.documentInitializer);
        const pdfBytes = await document.save();
        const pdfBlob = new Blob([pdfBytes], { type: 'application/pdf' });

        text.innerText = 'Done!';

        const a = window.document.createElement('a');
        a.href = URL.createObjectURL(pdfBlob);
        a.download = 'document.pdf';
        a.innerText = 'Download';
        dialog.appendChild(a);

        const closeButton = window.document.createElement('button');
        closeButton.innerText = 'Close';
        closeButton.onclick = () => {
          URL.revokeObjectURL(a.href);
          dialog.close();
          dialog.remove();
        };
        dialog.appendChild(closeButton);
      }}>
        <AppContext.Provider value={context}>
          <AttachmentsConfigurator />
          {context.documentInitializer instanceof WebDocumentInitializer && (<WebDocumentInitializerConfigurator />)}
          {context.documentInitializer instanceof FileDocumentInitializer && (<FileDocumentInitializerConfigurator />)}
          <DocumentInitializerPicker />
          <input type="submit" value="Generate PDF" />
        </AppContext.Provider>
      </form>
    </>
  );
}
