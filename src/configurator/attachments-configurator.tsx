import { useContext } from "preact/hooks";
import AppContext from "../app-context";
import { FileAttachment } from "../file-attachment";

export default function AttachmentsConfigurator() {
  const context = useContext(AppContext);


  return (
    <>
      {context.attachments.length === 0 ?
        <p>No attachments added yet.</p> :
        <ul>
          {context.attachments.map((attachment, index) => (
            <li><input type="button" onClick={() => {
              context.attachments.splice(index, 1);
              context.setContext({ ...context });
            }} value="Remove" /> {attachment.name}</li>
          ))}
        </ul>
      }
      <input type="button" onClick={() => {
        const input = window.document.createElement('input');
        input.type = 'file';
        input.multiple = true;
        input.onchange = async () => {
          const newAttachments = FileAttachment.fromInput(input);
          context.attachments.push(...newAttachments);
          context.setContext({ ...context });
        };
        input.click();
      }} value="Add attachment" />
    </>
  );
}
