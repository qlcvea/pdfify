import { useContext } from 'preact/hooks'
import AppContext, { AppContextContent } from '../../app-context';
import { FileDocumentInitializer } from '../../file-document-initializer';
import DocumentInitializer from '../../tool/document-initializer/document-initializer';
import WebDocumentInitializer from '../../web-document-initializer';

type RotationElement = { buttonText: string, check: (documentInitializer: DocumentInitializer) => boolean; use: (context: AppContextContent) => void }

const rotation = [
  {
    buttonText: 'Generate a placeholder page instead',
    check: (documentInitializer) => documentInitializer instanceof WebDocumentInitializer,
    use: (context: AppContextContent) => context.setContext({ ...context, documentInitializer: new WebDocumentInitializer() }),
  },
  {
    buttonText: 'Add attachments to an existing PDF instead',
    check: (documentInitializer) => documentInitializer instanceof FileDocumentInitializer,
    use: (context: AppContextContent) => {
      const input = window.document.createElement('input');
      input.type = 'file';
      input.accept = 'application/pdf';
      input.onchange = async () => {
        if (input.files !== null && input.files.length === 1) {
          const documentInitializer = await FileDocumentInitializer.fromFile(input.files[0]);
          context.setContext({ ...context, documentInitializer });
        }
      };
      input.click();
    }
  },
] as RotationElement[];

/**
 * A component that allows the user to pick a document initializer.
 * 
 * There is a rotation of possible document initializers. When the current one is identified, a button to switch to the next one will be shown.
 */
export default function DocumentInitializerPicker() {
  const context = useContext(AppContext);

  let nextRotation = null as RotationElement | null;
  for (let i = 0; i < rotation.length; i++) {
    if (rotation[i].check(context.documentInitializer)) {
      nextRotation = rotation[(i + 1) % rotation.length];
      break;
    }
  }

  if (nextRotation === null) {
    return null;
  } else {
    return (
      <input type="button" onClick={() => nextRotation?.use(context)} value={nextRotation.buttonText} />
    );
  }
}
