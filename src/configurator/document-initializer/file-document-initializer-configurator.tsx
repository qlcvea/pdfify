export default function FileDocumentInitializerConfigurator() {
  return (
    <>
      <p>These attachments will be added to the PDF you provided.</p>
    </>
  );
}
