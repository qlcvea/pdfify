import { useContext } from "preact/hooks";
import AppContext from "../../app-context";
import WebDocumentInitializer from "../../web-document-initializer";

export default function WebDocumentInitializerConfigurator() {
  const context = useContext(AppContext);

  if (!(context.documentInitializer instanceof WebDocumentInitializer)) return null;

  return (
    <>
      <label><input type="checkbox" checked={context.documentInitializer.includeBranding} onChange={(e) => {
        if (context.documentInitializer instanceof WebDocumentInitializer) {
          context.documentInitializer.includeBranding = e.currentTarget.checked;
          context.setContext({ ...context });
        }
      }} /> Include a link to this tool</label>
    </>
  );
}
