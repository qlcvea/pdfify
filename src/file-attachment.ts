import Attachment from "./tool/attachment";

/**
 * An Attachment sourced from a DOM {@link File} object.
 */
export class FileAttachment extends Attachment {
  private file: File;

  getContent(): Promise<ArrayBuffer> {
    return this.file.arrayBuffer();
  }

  /**
   * Create a new FileAttachment from a File object.
   * @param file The File object to wrap in this Attachment.
   */
  private constructor(file: File) {
    super(file.type);
    this.name = file.name;
    this.file = file;
  }

  /**
   * Create a new FileAttachment from a File object.
   * @param file The File object to wrap in this Attachment.
   * @returns A new FileAttachment that wraps the provided File.
   */
  static fromFile(file: File): FileAttachment {
    return new FileAttachment(file);
  }

  /**
   * Create a new FileAttachment from the files selected in an HTMLInputElement.
   * @param fileInput An HTMLInputElement of type "file" that contains the files to wrap in FileAttachments.
   * @returns An array of FileAttachments that wrap the files in the provided HTMLInputElement.
   * @remarks No error is thrown if the provided HTMLInputElement does not have any files selected. An empty array is returned instead.
   */
  static fromInput(fileInput: HTMLInputElement): FileAttachment[] {
    if (fileInput.type !== 'file' || fileInput.files === null) throw new Error('fileInput must be an HTMLInputElement of type "file"');
    let files: FileAttachment[] = [];
    for (let j = 0; j < fileInput.files.length; j++) {
      files.push(FileAttachment.fromFile(fileInput.files[j]));
    }
    return files;
  }
}