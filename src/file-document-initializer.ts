import Attachment from "./tool/attachment";
import TemplatedDocumentInitializer from "./tool/document-initializer/templated-document-initializer";

/**
 * A {@link TemplatedDocumentInitializer} where the template is sourced from a DOM {@link File} object.
 */
export class FileDocumentInitializer extends TemplatedDocumentInitializer {
  private constructor(arrayBuffer: ArrayBuffer) {
    super(arrayBuffer);
  }

  /**
   * Create a new {@link FileDocumentInitializer} from a File object.
   * @param file The File object to create a {@link FileDocumentInitializer} from.
   * @returns A {@link FileDocumentInitializer} that will create copies of the provided File.
   */
  static async fromFile(file: File): Promise<FileDocumentInitializer> {
    return new FileDocumentInitializer(await file.arrayBuffer());
  }

  /**
   * Create a new {@link FileDocumentInitializer} from the files selected in an HTMLInputElement.
   * @param fileInput An HTMLInputElement of type "file" that contains the file to create the {@link FileDocumentInitializer} from.
   * @returns A {@link FileDocumentInitializer} that will create copies of the file in the provided HTMLInputElement.
   * @throws An error if the provided HTMLInputElement does not have exactly one file selected.
   */
  static async fromInput(fileInput: HTMLInputElement): Promise<FileDocumentInitializer> {
    if (fileInput.type !== 'file' || fileInput.files === null) throw new Error('fileInput must be an HTMLInputElement of type "file"');
    if (fileInput.files.length !== 1) throw new Error('fileInput must have exactly one file selected');

    return await FileDocumentInitializer.fromFile(fileInput.files[0]);
  }
}