import { PrivacyLink } from "./privacy-link";

export function Header() {
  return (
    <>
      <h1>pdfify</h1>
      <p>
        <i>A tool to bundle up files as attachments to a PDF document. Created using <a href="https://pdf-lib.js.org" target="_blank">pdf-lib</a>.</i><br />
        <a href="licenses.txt" target="_blank">Open source licenses</a> - <a href="https://gitlab.com/qlcvea/pdfify" target="_blank">Source code</a> - <PrivacyLink />
      </p>
      <hr />
    </>
  )
}
