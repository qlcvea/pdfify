import { createRef } from "preact"

export function PrivacyLink() {
  const dialogRef = createRef<HTMLDialogElement>();

  return (
    <>
      <a href="#privacy" onClick={(e) => {
        e.preventDefault();
        if (dialogRef.current !== null) {
          dialogRef.current.style.display = '';
          dialogRef.current.showModal();
        }
      }}>Privacy info</a>
      <dialog ref={dialogRef} style="display:none">
        <p>The webapp is served using <a href="https://about.gitlab.com/stages-devops-lifecycle/pages/" target="_blank">GitLab Pages</a>. GitLab may keep information sent by your browser upon your visit to this website according with <a href="https://about.gitlab.com/privacy/" target="_blank">their privacy policy</a>.</p>
        <p>None of the files added as attachments or generated using this webapp are transmitted to third parties by this webapp. If you wish to share documents generated using this webapp, you have to do so yourself.</p>
        <p><button onClick={() => {
          if (dialogRef.current !== null) {
            dialogRef.current.style.display = 'none';
            dialogRef.current.close();
          }
        }}>Close</button></p>
      </dialog>
    </>
  )
}
