/**
 * A file to be included in the output.
 */
export default abstract class Attachment {
  /**
   * Filename.
   * 
   * @remarks This value should include the file extension.
   * @remarks This value should not be treated as constant.
   * @remarks This value should be set by implementations when creating the object.
   */
  name: string = 'Unnamed';

  private _type: string;
  /**
   * MIME type.
   * 
   * @remarks This value should be set by implementations when creating the object.
   * @remarks This value is constant.
   */
  get type(): string {
    return this._type;
  };

  constructor(type: string) {
    this._type = type;
  }

  /**
   * Retrieve the attachment's content.
   * @returns The attachment's content, or a Promise that resolves to the attachment's content.
   */
  abstract getContent(): Uint8Array | ArrayBuffer | Promise<Uint8Array | ArrayBuffer>;
}