import { PDFDocument } from "pdf-lib";
import DocumentInitializer from "./document-initializer";

/**
 * This class provides a generator to initialize an empty {@link PDFDocument}.
 */
export default class BasicDocumentInitializer extends DocumentInitializer {
  createNew(): Promise<PDFDocument> {
    return PDFDocument.create();
  }
}