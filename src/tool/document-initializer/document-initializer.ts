import { PDFDocument } from "pdf-lib";

/**
 * This class provides a generator to initialize a {@link PDFDocument}.
 * 
 * @remarks This class exists to be able to have different sources of initial PDFs.
 */
export default abstract class DocumentInitializer {
  /**
   * Generate a new {@link PDFDocument} to add attachments to.
   * @returns A new PDFDocument.
   */
  abstract createNew(): PDFDocument | Promise<PDFDocument>;
}