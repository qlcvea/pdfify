import { PDFDocument } from "pdf-lib";
import DocumentInitializer from "./document-initializer";

/**
 * This class provides a generator to initialize a {@link PDFDocument} by copying another PDF.
 */
export default class TemplatedDocumentInitializer extends DocumentInitializer {
  private template: Uint8Array | ArrayBuffer;

  /**
   * Create a new {@link TemplatedDocumentInitializer} from a source file.
   * @param template The template file to create other files from.
   */
  constructor(template: Uint8Array | ArrayBuffer) {
    super();
    this.template = template;
  }

  createNew(): Promise<PDFDocument> {
    return PDFDocument.load(this.template);
  }
}