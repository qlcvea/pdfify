import { PDFDocument } from "pdf-lib";
import Attachment from "./attachment";
import DocumentInitializer from "./document-initializer/document-initializer";

/**
 * Bundle up files as attachments to a PDF document.
 * @param attachments The attachments to include in the document.
 * @param documentInitializer The {@link DocumentInitializer} to use to create the document that the attachments will be added to.
 * @returns A Promise that resolves to the generated PDF document.
 */
async function pdfify(attachments: Attachment[], documentInitializer: DocumentInitializer): Promise<PDFDocument> {
  const document = await documentInitializer.createNew();

  // Cue up a bunch of promises to await at the end for parallelization
  let promises = [];

  for (let attachment of attachments) {
    promises.push((async () => await document.attach(await attachment.getContent(), attachment.name, { mimeType: attachment.type }))());
  }

  await Promise.all(promises);

  return document;
}

export default pdfify;