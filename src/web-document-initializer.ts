import { PageSizes, PDFDocument, PDFName, PDFPage, PDFString, rgb, StandardFonts } from "pdf-lib";
import DocumentInitializer from "./tool/document-initializer/document-initializer";

// There is no high-level API to add links, see https://github.com/Hopding/pdf-lib/issues/555#issuecomment-670241308
const createPageLinkAnnotation = (page: PDFPage, uri: string, location: number[]) =>
page.doc.context.register(
  page.doc.context.obj({
    Type: 'Annot',
    Subtype: 'Link',
    Rect: [...location],
    Border: [0, 0, 0],
    C: [0, 0, 0],
    A: {
      Type: 'Action',
      S: 'URI',
      URI: PDFString.of(uri),
    },
  }),
);

/**
 * This is the default generator for the webapp. It creates a PDF with one page.
 */
export default class WebDocumentInitializer extends DocumentInitializer {
  /**
   * Include a link to the current page in the generated document.
   */
  includeBranding: boolean = true;

  async createNew(): Promise<PDFDocument> {
    const document = await PDFDocument.create();
    const font = await document.embedFont(StandardFonts.Helvetica);

    const page = await document.addPage(PageSizes.A4);
    page.setFont(font);
    const text = 'This PDF only contains attachments.';
    const size = 20;
    const heightWithDescender = font.heightAtSize(size, { descender: true });
    const width = font.widthOfTextAtSize(text, size);
    const x = page.getWidth() / 2 - width / 2;
    const y = page.getHeight() / 2 - heightWithDescender / 2;
    page.drawText('This PDF only contains attachments.', { font, x, y, size });

    if (this.includeBranding) {
      const text = window.location.hostname;
      const size = 10;
      const width = font.widthOfTextAtSize(text, size);
      const height = font.heightAtSize(size, { descender: false });
      const heightWithDescender = font.heightAtSize(size, { descender: true });
      const descender = heightWithDescender - height;
      const x = page.getSize().width - width - 10; // Right align
      const y = 10;
      page.drawText(text, { font, x, y, size, color: rgb(0, 0, 1) });
      const link = createPageLinkAnnotation(page, window.location.origin, [ x, y - descender, x + width, y + heightWithDescender]);
      page.node.set(PDFName.of('Annots'), document.context.obj([link]));
    }

    return document;
  }
}